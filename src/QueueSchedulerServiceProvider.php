<?php

namespace Drupal\queue_scheduler;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\queue_scheduler\Queue\QueueDatabaseFactory;

/**
 * Registers services in the container.
 */
class QueueSchedulerServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $service_definition = $container->getDefinition('queue.database');
    $service_definition->setClass(QueueDatabaseFactory::class);
    $container->setDefinition('queue.database', $service_definition);
  }

}
