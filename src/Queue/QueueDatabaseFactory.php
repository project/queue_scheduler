<?php

namespace Drupal\queue_scheduler\Queue;

use Drupal\Core\Queue\QueueDatabaseFactory as CoreQueueDatabaseFactory;

/**
 * {@inheritdoc}
 */
class QueueDatabaseFactory extends CoreQueueDatabaseFactory {

  /**
   * {@inheritdoc}
   */
  public function get($name) {
    return new DatabaseQueueScheduler($name, $this->connection);
  }

}
