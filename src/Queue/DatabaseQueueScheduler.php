<?php

namespace Drupal\queue_scheduler\Queue;

use Drupal\Core\Queue\DatabaseQueue;

/**
 * {@inheritdoc}
 */
class DatabaseQueueScheduler extends DatabaseQueue {

  /**
   * {@inheritdoc}
   */
  public function createItemScheduled($data, $process_at) {
    $try_again = FALSE;
    try {
      $id = $this->doCreateItemScheduled($data, $process_at);
    } catch (\Exception $e) {
      // If there was an exception, try to create the table.
      if (!$try_again = $this->ensureTableExists()) {
        // If the exception happened for other reason than the missing table,
        // propagate the exception.
        throw $e;
      }
    }
    // Now that the table has been created, try again if necessary.
    if ($try_again) {
      $id = $this->doCreateItemScheduled($data, $process_at);
    }
    return $id;
  }

  /**
   * Adds a queue item and store it directly to the queue.
   *
   * @param $data
   *   Arbitrary data to be associated with the new task in the queue.
   * @param $process_at
   *   The timestamp when must be processed the item.
   *
   * @return
   *   A unique ID if the item was successfully created and was (best effort)
   *   added to the queue, otherwise FALSE. We don't guarantee the item was
   *   committed to disk etc, but as far as we know, the item is now in the
   *   queue.
   */
  protected function doCreateItemScheduled($data, $process_at) {
    $query = $this->connection->insert(static::TABLE_NAME)
      ->fields([
        'name' => $this->name,
        'data' => serialize($data),
        // We cannot rely on REQUEST_TIME because many items might be created
        // by a single request which takes longer than 1 second.
        'created' => \Drupal::time()->getCurrentTime(),
        'process_at' => $process_at,
      ]);
    // Return the new serial ID, or FALSE on failure.
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function claimItem($lease_time = 30) {
    // Claim an item by updating its expire fields. If claim is not successful
    // another thread may have claimed the item in the meantime. Therefore loop
    // until an item is successfully claimed or we are reasonably sure there
    // are no unclaimed items left.
    while (TRUE) {
      try {
        $item = $this->connection->queryRange('SELECT [data], [created], [item_id] FROM {' . static::TABLE_NAME . '} q WHERE [expire] = 0 AND [name] = :name AND [process_at] = 0 ORDER BY [created], [item_id] ASC', 0, 1, [':name' => $this->name])
          ->fetchObject();
      } catch (\Exception $e) {
        $this->catchException($e);
      }

      // If the table does not exist there are no items currently available to
      // claim.
      if (empty($item)) {
        return FALSE;
      }

      // Try to update the item. Only one thread can succeed in UPDATEing the
      // same row. We cannot rely on REQUEST_TIME because items might be
      // claimed by a single consumer which runs longer than 1 second. If we
      // continue to use REQUEST_TIME instead of the current time(), we steal
      // time from the lease, and will tend to reset items before the lease
      // should really expire.
      $update = $this->connection->update(static::TABLE_NAME)
        ->fields([
          'expire' => \Drupal::time()->getCurrentTime() + $lease_time,
        ])
        ->condition('item_id', $item->item_id)
        ->condition('expire', 0);
      // If there are affected rows, this update succeeded.
      if ($update->execute()) {
        $item->data = unserialize($item->data);
        return $item;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function claimItemScheduled($lease_time = 30, $force = FALSE) {
    // Claim an item by updating its expire fields. If claim is not successful
    // another thread may have claimed the item in the meantime. Therefore loop
    // until an item is successfully claimed or we are reasonably sure there
    // are no unclaimed items left.
    while (TRUE) {
      try {
        $item = $this->connection->queryRange('SELECT [data], [created], [item_id] FROM {' . static::TABLE_NAME . '} q WHERE [expire] = 0 AND [name] = :name AND [process_at] <> 0 AND [process_at] <= :time ORDER BY [created], [item_id] ASC', 0, 1, [
          ':name' => $this->name,
          ':time' => $force ? 9999999999 : time(),
        ])->fetchObject();
      } catch (\Exception $e) {
        $this->catchException($e);
      }

      // If the table does not exist there are no items currently available to
      // claim.
      if (empty($item)) {
        return FALSE;
      }

      // Try to update the item. Only one thread can succeed in UPDATEing the
      // same row. We cannot rely on REQUEST_TIME because items might be
      // claimed by a single consumer which runs longer than 1 second. If we
      // continue to use REQUEST_TIME instead of the current time(), we steal
      // time from the lease, and will tend to reset items before the lease
      // should really expire.
      $update = $this->connection->update(static::TABLE_NAME)
        ->fields([
          'expire' => \Drupal::time()->getCurrentTime() + $lease_time,
        ])
        ->condition('item_id', $item->item_id)
        ->condition('expire', 0);
      // If there are affected rows, this update succeeded.
      if ($update->execute()) {
        $item->data = unserialize($item->data);
        return $item;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function schemaDefinition() {
    $schema = parent::schemaDefinition();
    $schema['fields']['process_at'] = [
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
      'description' => 'Timestamp when the item must be processed.',
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function loadItem($item) {
    try {
      $item = $this->connection->queryRange('SELECT [data], [created], [item_id] FROM {' . static::TABLE_NAME . '} q WHERE [expire] = 0 AND [name] = :name AND [item_id] = :item ORDER BY [created], [item_id] ASC', 0, 1, [
        ':name' => $this->name,
        ':item' => $item,
      ])
        ->fetchObject();
    } catch (\Exception $e) {
      $this->catchException($e);
    }

    // If the table does not exist there are no items currently available to
    // claim.
    if (empty($item)) {
      return FALSE;
    }
  }

}
